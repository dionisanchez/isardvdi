# https://docs.gitlab.com/ee/ci/yaml/index.html#switch-between-branch-pipelines-and-merge-request-pipelines
workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - when: always

stages:
 - docker-compose
 - docker-images
 - docker-registry
 - changelog
 - force-changelog-retry

docker-compose:
  image: docker:latest
  stage: docker-compose
  services:
    - docker:dind
  script:
    - apk add bash git docker-compose
    # Use docker compose < 1.27 to avoid depends_on issue
    # https://github.com/isard-vdi/isard/issues/390
    - apk add py3-pip
    - pip install docker-compose~=1.26.0
    - cp isardvdi.cfg.example isardvdi.cfg
    - echo "DOCKER_IMAGE_PREFIX=${CI_REGISTRY_IMAGE}/" >> isardvdi.cfg
    - echo "DOCKER_IMAGE_TAG=$CI_COMMIT_REF_SLUG" >> isardvdi.cfg
    - cp isardvdi.cfg isardvdi.build.cfg
    - echo "USAGE=build" >> isardvdi.build.cfg
    - cp isardvdi.build.cfg isardvdi.video-standalone.build.cfg
    - echo "FLAVOUR=video-standalone" >> isardvdi.video-standalone.build.cfg
    - cp isardvdi.build.cfg isardvdi.toolbox.build.cfg
    - echo "FLAVOUR=toolbox" >> isardvdi.toolbox.build.cfg
    - ./build.sh
  artifacts:
    paths:
      - docker-compose*.yml

docker-image:
  image: docker:latest
  stage: docker-images
  services:
    - docker:dind
  parallel:
    matrix:
      - DOCKER_COMPOSE: build
        IMAGE:
          - api
          - authentication
          - engine
          - grafana
          - guac
          - hypervisor
          - portal
          - squid
          - static
          - stats
          - vpn
          - webapp
          - websockify
      - DOCKER_COMPOSE: video-standalone.build
        IMAGE: video
      - DOCKER_COMPOSE: toolbox.build
        IMAGE: toolbox
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - apk add git docker-compose
    - git submodule update --init --recursive --remote
    - docker-compose -f docker-compose.$DOCKER_COMPOSE.yml build isard-$IMAGE
    - docker tag $CI_REGISTRY_IMAGE/$IMAGE:$CI_COMMIT_REF_SLUG $CI_REGISTRY_IMAGE/$IMAGE:git-$CI_COMMIT_SHORT_SHA
    - docker push $CI_REGISTRY_IMAGE/$IMAGE:git-$CI_COMMIT_SHORT_SHA

docker-tag:
  stage: docker-registry
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  parallel:
    matrix:
      - IMAGE:
          - api
          - authentication
          - engine
          - grafana
          - guac
          - hypervisor
          - portal
          - squid
          - static
          - stats
          - vpn
          - webapp
          - websockify
          - video
          - toolbox
  script:
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - crane tag $CI_REGISTRY_IMAGE/$IMAGE:git-$CI_COMMIT_SHORT_SHA $CI_COMMIT_REF_SLUG

.changelog:
  image: registry.gitlab.com/gitlab-org/release-cli
  stage: changelog
  before_script:
    - apk add --no-cache nodejs npm git
    - cd releaser && npm i --save && cd -

changelog:
  extends: .changelog
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
    - ./releaser/index.js CHANGELOG.md

changelog-release:
  extends: .changelog
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  script:
    - url_host=`git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g"`
    - git remote set-url origin "https://${GITLAB_LOGIN}:${GITLAB_TOKEN}@${url_host}"
    - ./releaser/index.js CHANGELOG.md --release

force-changelog-retry:
  image: alpine
  stage: force-changelog-retry
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: always
      allow_failure: true
    - when: never
  before_script:
    - apk add --no-cache jq curl
  script:
    - export gitlab_base_url="https://gitlab.com/api/v4/projects"
    - |
      export opened_merge_requests=`curl -H "Authorization: Bearer ${GITLAB_TOKEN}" "${gitlab_base_url}/${PROJECT_ID}/merge_requests?state=opened"`
    - |
      for iid in $(echo "${opened_merge_requests}" | jq '.[] | .iid'); do
        latest_pipeline=`curl -H "Authorization: Bearer ${GITLAB_TOKEN}" "${gitlab_base_url}/${PROJECT_ID}/merge_requests/${iid}/pipelines" |  jq '.[0] | .id'`
        latest_changelog_job=`curl -H "Authorization: Bearer ${GITLAB_TOKEN}" "${gitlab_base_url}/${PROJECT_ID}/pipelines/${latest_pipeline}" |  jq '.[] | select(.name == "changelog") | .id' `
        curl -x POST -H "Authorization: Bearer ${GITLAB_TOKEN}" "$gitlab_base_url/${PROJECT_ID}/jobs/${latest_changelog_job}/retry"
      done
